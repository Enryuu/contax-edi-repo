package com.as2.modify;

import java.io.IOException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.io.InputStream;

import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.openssl.PEMException;
import org.bouncycastle.util.io.pem.PemObject;
import com.helger.as2lib.util.cert.CertificateException;

public class CertificateManagement {
	
	/**
     * Converts a PEM formatted String to a {@link X509Certificate} instance.
     *
     * @param pem PEM formatted String
     * @return a X509Certificate instance
     * @throws CertificateException 
     * @throws IOException
     */
	
    public static X509Certificate eee(){
		return null;}
	
    public static X509Certificate convertToX509Certificate(InputStream pem) throws CertificateException, IOException 
    {
    	
    	
    	X509Certificate cert = null;
		try {
			CertificateFactory cFactory = CertificateFactory.getInstance("X.509"); 
			cert = (X509Certificate) cFactory.generateCertificate(pem);
		} catch (java.security.cert.CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//        X509Certificate cert = null;
//        StringReader reader = new StringReader(pem);
//        PemReader pemReader = new PemReader(reader);
//        PemObject pem1 = pemReader.readPemObject();
//        cert = (X509Certificate) CertificateManagement.parseObject(pem1);
//        
        
        return cert;
    }

    	
    public static Object parseObject(PemObject obj)
    	    throws IOException
    	{
    	    try
    	    {
    	        return new X509CRLHolder(obj.getContent());
    	    }
    	    catch (Exception e)
    	    {
    	        throw new PEMException("problem parsing cert: " + e.toString(), e);
    	    }
    	}
}
