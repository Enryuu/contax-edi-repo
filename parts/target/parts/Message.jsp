<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<form name="sendMessage" enctype="multipart/form-data" action="<%=request.getContextPath()%>/sendmsg" method="post">

<fieldset>
<legend>Send from Partner:</legend>


<label>Partner name: </label> <input type="text"  name="partner" value=""><br>
<label>AS2 Identifer: </label>   <input type="text" placeholder ="Same name used as certificate alias" name="sendID" value=""><br>
<label>AS2-Email: </label> <input width ="200px" name="sendMail" ><br>
<label>AS2 Alias </label> <input type = "text"  name="sendKey" value=""><br>


</fieldset>


<fieldset>
<legend>Send to Partner:</legend>


<label>Partner name: </label> <input type="text"  name="partner" value=""><br>
<label>AS2 Identifer: </label>   <input type="text" placeholder ="Same name used as certificate alias" name="recieveID" value=""><br>
<label>Alias </label> <input type ="text" name="receiveAlias" value=""><br>
<label>Partner URL: </label> <input type = "text"  name="recieveURL" value=""><br>

<fieldset>

<fieldset>
<legend>Message:  </legend>

<label>Subject: </label> <input type = "text"  name="subject" value=""><br>
<label>File:</label> <input type ="file" name="payload"  >
<label>Sign options:</label> <select name="signALG" >

<option value="md5">MD5</option>
<option value="sha-1">SHA1</option>
<option value="sha-224">SHA224</option>
<option value="sha-256">SHA256</option>
<option value="sha-384">SHA384</option>
<option value="sha-512">SHA512</option>



</select>
</fieldset>

<legend>Message options:</legend>

	<input type="checkbox" name="sign" value=""><label>Sign Messages - Whether to sign the messages sent to this partner</label><br>
	<input type="checkbox" name="encrypt" value=""><label>Encrypt Messages - Whether to encrypt the messages sent to this partner</label><br>
	<input type="checkbox" name="compress_before" value=""><label>Compress Before - Whether to compress the messages before signing and/or encryption</label><br>
	<input type="checkbox" name="compress_after" value=""><label>Compress After - Whether to compress the messages after signing and/or encryption</label><br><br>
	
</fieldset>

<fieldset>

<legend>MDN options:</legend>

	<input type="checkbox" name="MDN_req" value=""><label>Request MDN</label><br>
	<input type="checkbox" name="MDN_aSyc_req" value=""><label>Request Asynchronous MDN request</label><br>
	<input type="checkbox" name="MDN_signed" value=""><label>Request signed MDN</label><br>

	
</fieldset>
</fieldset>

<button type="submit" value = "submit" >Submit</button>


</form>

</body>
</html>